from django.conf.urls import url

from . import views

app_name = 'equipment'
urlpatterns = [
    url(
        r'^$',
        views.EquipmentView.as_view(),
        name='index'
    ),
    url(
        r'^data/list/$',
        views.EquipmentData,
        name='equipment-data-list'
    ),
    # url(
    #     r'^list/$',
    #     views.EquipmentListView.as_view(),
    #     name='equipment-list'
    # ),
    url(
        r'^details/(?P<id>[-\w]+)/$',
        views.EquipmentDetailView.as_view(),
        name='equipment-detail'
    ),
    url(
        r'^details/(?P<id>[-\w]+)/data/$',
        views.EquipmentDetailData,
        name='equipment-detail-data'
    ),
    url(
        r'^kpm/$',
        views.KeyPerformanceMonitoringListView.as_view(),
        name='kpm-list'
    ),
    url(
        r'^kpm/data/',
        views.KeyPerformanceMonitoringListData,
        name='kpm-list-data'
    ),
    url(
        r'^kpm/[a-zA-Z0-9_-]*/[a-zA-Z0-9_-]*/[a-zA-Z0-9_-]*/$',
        views.KeyPerformanceMonitoringDetailView.as_view(),
        name='kpm-detail'
    ),
    url(
        r'^kpm/(?P<eid>[-\w]+)/(?P<drs>[-\w]+)/(?P<dre>[-\w]+)/data/',
        views.KeyPerformanceMonitoringDetailData,
        name='kpm-detail-data'
    ),
]

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView

from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.utils import timezone

from django.http import HttpResponse, HttpResponseNotFound

from .models import *

from datetime import datetime, date

import json
import calendar

from dw.aux import DatabaseAux

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial
    else:
        return obj.__str__()


@method_decorator(login_required, name='dispatch')
class EquipmentView(TemplateView):
    model = Equipment
    template_name = 'equipment/index.html'


def EquipmentData(request):
    queryset = Equipment.objects.all()
    resp = {}
    data = [
        {
            'id': item.id,
            'pn': item.propertyno,
            'mn': item.unit_model.unit_model,
            'sn': item.unit_serial_no,
            'sc': item.status.status_name,
            'lc': item.location.location_name,
        } for item in queryset
    ]
    resp['data'] = data
    return HttpResponse(
        json.dumps(
            resp
        ),
        content_type='application/json'
    )


@method_decorator(login_required, name='dispatch')
class EquipmentListView(TemplateView, ListView):
    model = Equipment
    template_name = 'equipment/equipment_list.html'

    def get_context_data(self, **kwargs):
        context = super(EquipmentListView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context


# @method_decorator(login_required, name='dispatch')
# class EquipmentDetailView(DetailView):
#     model = Equipment
#     template_name = 'equipment/equipment_detail.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(EquipmentDetailView, self).get_context_data(**kwargs)
#         context['now'] = timezone.now()
#         return context

@method_decorator(login_required, name='dispatch')
class EquipmentDetailView(TemplateView):
    template_name = 'equipment/equipment_detail.html'

def EquipmentDetailData(request, id):
    e = Equipment.objects.filter(id=id)
    u = Utilization.objects.filter(equipment=id)
    j = JobOrder.objects.filter(equipment=id)
    p = MaintenancePlan.objects.filter(equipment=id)
    m = Maintenance.objects.filter(equipment=id)

    resp = {}
    e = [
        {
            'id': item.id,
            'pn': item.propertyno,
            'mn': item.unit_model.unit_model,
            'sn': item.unit_serial_no,
            'sc': item.status.status_name,
            'lc': item.location.location_name,
            'suc': item.supplier.supplier_name,
            'pc': "{0:.2f}".format(item.purchase_cost),
            'rr': "{0:.2f}".format(item.rental_rate),
            'tpc': item.third_party_inspection_certificate,
            'toh': item.total_operating_hours,
            'tmr': item.total_mileage
        } for item in e
    ]

    u = [
        {
            'id': i.id,
            'ded': i.dedupid,
            'dud': json_serial(i.date_used),
            'tsh': i.shift_hours,
            'tdh': i.down_hours,
            'tah': i.shift_hours - i.down_hours,
            'toh': i.operating_hours,
            'tih': (i.shift_hours - i.down_hours) - i.operating_hours,
            'tch': i.chargable_hours
        } for i in u
    ]

    j = [
        {
            'id': i.id,
            'jon': i.job_order_no,
            'dc': json_serial(i.date_file),
            'jos': i.summary,
            'jas': i.activities,
            'ds': json_serial(i.date_start),
            'df': json_serial(i.date_completed)
        } for i in j
    ]

    p = [
        {
            'id': i.id,
            'ded': i.dedupid,
            'dps': json_serial(i.date_start),
            'dpe': json_serial(i.date_end),
            'oht': i.operating_hours_threshold,
            'ohtw': i.operating_hours_threshold_warn,
            'mt': i.mileage_threshold,
            'mtw': i.mileage_threshold_warn
        } for i in p
    ]

    m = [
        {
            'id': i.id,
            'ded': i.dedupid,
            'ppm': json_serial(i.date_last),
            'npm': json_serial(i.date_next),
            'poh': i.last_operating_hours,
            'pmr': i.last_mileage_reading,
            'pmp': i.passed_test
        } for i in m
    ]

    resp = {
        'detail': e,
        'utilization': u,
        'joborder': j,
        'pmplan': p,
        'pmrecord': m
    }

    return HttpResponse(
        json.dumps(
            resp
        ),
        content_type='application/json'
    )



# PM and KPM Section
@method_decorator(login_required, name='dispatch')
class PreventiveMaintenanceListView(TemplateView, ListView):
    pass

@method_decorator(login_required, name='dispatch')
class KeyPerformanceMonitoringListView(TemplateView):
    template_name = 'equipment/kpm_list.html'

@method_decorator(login_required, name='dispatch')
class KeyPerformanceMonitoringDetailView(TemplateView):
    template_name = 'equipment/kpm_detail.html'


def KeyPerformanceMonitoringListData(request):
    """Generate data for KPM List"""
    aux = DatabaseAux()
    req = {
        'd_start': datetime.strptime(request.GET['d_start'], '%Y-%m-%d').date(),
        'd_end': datetime.strptime(request.GET['d_end'], '%Y-%m-%d').date(),
    }
    print(req['d_start'])
    print(req['d_end'])
    kpm = aux.get_kpm_matrix(d_start=req['d_start'],d_end=req['d_end'])
    resp = {}
    resp['data'] = kpm
    return HttpResponse(
        json.dumps(
            resp
        ),
        content_type='application/json'
    )

def KeyPerformanceMonitoringDetailData(request, eid, drs, dre):
    aux = DatabaseAux()
    if eid is None:
        pass
    else:
        resp = {}
        kpm = aux.get_kpm_matrix(
            uid=[eid],
            d_start=datetime.strptime(drs, '%Y-%m-%d').date(),
            d_end=datetime.strptime(dre, '%Y-%m-%d').date()
        )
        resp['summary'] = kpm
        uset = Utilization.aux.get_utilization_period(
            eid,
            datetime.strptime(drs, '%Y-%m-%d').date(),
            datetime.strptime(dre, '%Y-%m-%d').date()
        )
        mur = [{
            'id': u['id'],
            'ded': u['ded'],
            'dud': json_serial(u['dud']),
            'tsh': u['tsh'],
            'tdh': u['tdh'],
            'tah': u['tah'],
            'toh': u['toh'],
            'tih': u['tih'],
            'tch': u['tch']
        } for u in uset ]
        resp['utilization'] = mur
    return HttpResponse(
        json.dumps(
            resp
        ),
        content_type='application/json'
    )

# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-10-21 18:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0009_auto_20161021_1838'),
    ]

    operations = [
        migrations.AlterField(
            model_name='equipment',
            name='description',
            field=models.TextField(blank=True, max_length=1024, null=True),
        ),
        migrations.AlterField(
            model_name='equipment',
            name='third_party_inspection_certificate',
            field=models.CharField(default='NOT_CERTIFIED', max_length=64),
        ),
        migrations.AlterField(
            model_name='equipmentmodel',
            name='description',
            field=models.TextField(blank=True, max_length=1024, null=True),
        ),
        migrations.AlterField(
            model_name='equipmentstatus',
            name='description',
            field=models.TextField(blank=True, max_length=1024, null=True),
        ),
        migrations.AlterField(
            model_name='location',
            name='description',
            field=models.TextField(blank=True, max_length=1024, null=True),
        ),
        migrations.AlterField(
            model_name='supplier',
            name='description',
            field=models.TextField(blank=True, max_length=1024, null=True),
        ),
    ]

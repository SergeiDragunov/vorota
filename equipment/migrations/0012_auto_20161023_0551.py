# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-10-23 05:51
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0011_auto_20161023_0419'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='location',
            options={'ordering': ['location_name', 'location_code']},
        ),
        migrations.AddField(
            model_name='supplier',
            name='supplier_code',
            field=models.CharField(default='FIXME', max_length=16, unique=True),
        ),
        migrations.AlterField(
            model_name='equipment',
            name='location',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='equipment.Location'),
        ),
        migrations.AlterField(
            model_name='equipment',
            name='propertyno',
            field=models.CharField(max_length=32, unique=True),
        ),
        migrations.AlterField(
            model_name='equipmentmodel',
            name='unit_model',
            field=models.CharField(default='FIXME', max_length=512, unique=True),
        ),
        migrations.AlterField(
            model_name='equipmentstatus',
            name='status_code',
            field=models.CharField(default='FIXME', max_length=8, unique=True),
        ),
        migrations.AlterField(
            model_name='equipmentstatus',
            name='status_name',
            field=models.CharField(default='FIXME', max_length=32, unique=True),
        ),
        migrations.AlterField(
            model_name='equipmenttype',
            name='type_code',
            field=models.CharField(max_length=32, unique=True),
        ),
        migrations.AlterField(
            model_name='equipmenttype',
            name='type_name',
            field=models.CharField(max_length=32, unique=True),
        ),
        migrations.AlterField(
            model_name='joborder',
            name='job_order_no',
            field=models.CharField(max_length=32, unique=True),
        ),
        migrations.AlterField(
            model_name='location',
            name='location_code',
            field=models.CharField(default='FIXME', max_length=16, unique=True),
        ),
        migrations.AlterField(
            model_name='location',
            name='location_name',
            field=models.CharField(default='FIXME', max_length=254, unique=True),
        ),
        migrations.AlterField(
            model_name='supplier',
            name='supplier_name',
            field=models.CharField(max_length=64, unique=True),
        ),
    ]

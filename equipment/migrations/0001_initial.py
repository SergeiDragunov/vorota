# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-10-10 10:51
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Equipment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('propertyno', models.CharField(max_length=32)),
                ('unit_serial_no', models.CharField(max_length=128)),
                ('purchase_cost', models.DecimalField(decimal_places=2, max_digits=16)),
            ],
        ),
        migrations.CreateModel(
            name='EquipmentModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('unit_brand', models.CharField(blank=True, max_length=64, null=True)),
                ('unit_model', models.CharField(blank=True, max_length=512, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='EquipmentStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status_code', models.CharField(blank=True, max_length=8, null=True)),
                ('status_name', models.CharField(blank=True, max_length=32, null=True)),
            ],
            options={
                'verbose_name_plural': 'Equipment Statuses',
            },
        ),
        migrations.CreateModel(
            name='JobOrder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('job_order_no', models.CharField(max_length=32)),
                ('historydate', models.DateField()),
                ('activities', models.CharField(max_length=1024)),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('location_code', models.CharField(blank=True, max_length=16, null=True)),
                ('location_name', models.CharField(blank=True, max_length=254, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Supplier',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('supplier_name', models.CharField(max_length=64)),
                ('supplier_desc', models.CharField(max_length=512)),
            ],
        ),
        migrations.AddField(
            model_name='equipment',
            name='location',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='equipment.Location'),
        ),
        migrations.AddField(
            model_name='equipment',
            name='status',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='equipment.EquipmentStatus'),
        ),
        migrations.AddField(
            model_name='equipment',
            name='unit_model',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='equipment.EquipmentModel'),
        ),
    ]

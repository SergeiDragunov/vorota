from django.contrib import admin

from .models import Equipment

from .models import EquipmentType
from .models import EquipmentModel
from .models import EquipmentStatus

from .models import Location
from .models import Supplier

from .models import JobOrder

from .models import Utilization

from .models import MaintenanceType
from .models import MaintenancePlan
from .models import Maintenance

from .models import Projects
from .models import Deployments

class EquipmentTypeAdmin(admin.ModelAdmin):
    list_display = (
        'type_code', 'type_name'
    )


class EquipmentModelAdmin(admin.ModelAdmin):
    list_display = (
        '__str__', 'equipment_type', 'unit_model', 'unit_brand',
    )
    search_fields = [
        'unit_brand', 'unit_model',
    ]


class EquipmentAdmin(admin.ModelAdmin):
    list_display = (
        'propertyno', 'unit_model', 'unit_serial_no',
        'status', 'location',
        'rental_rate', 'total_operating_hours',
    )
    list_filter = (
        'status',
    )
    search_fields = [
        'propertyno', 'unit_serial_no',
        'unit_model__unit_model', 'location__location_name',
    ]

    def get_location_type(self, obj):
        return obj.location.location_type

    get_location_type.short_description = 'Location'
    get_location_type.admin_order_field = 'location__location_type'


class EquipmentModelAdmin(admin.ModelAdmin):
    list_display = (
        '__str__', 'equipment_type', 'unit_model', 'unit_brand',
    )
    search_fields = [
        'unit_brand', 'unit_model',
    ]


class LocationAdmin(admin.ModelAdmin):
    list_display = (
        'location_name', 'location_code', 'location_type',
    )
    search_fields = [
        'location_name', 'location_code',
    ]

class SupplierAdmin(admin.ModelAdmin):
    list_display = (
        'supplier_name', 'supplier_code',
        'account_manager', 'contact_number', 'email'
    )
    search_fields = [
        'supplier_name', 'supplier_code',
    ]


class JobOrderAdmin(admin.ModelAdmin):
    list_display = (
        'job_order_no', 'date_file', 'equipment', 'summary'
    )
    search_fields = [
        'job_order_no', 'date_file', 'equipment__propertyno',
        'summary'
    ]
    date_hierarchy = 'date_file'


class UtilizationAdmin(admin.ModelAdmin):
    list_display = (
        'date_used', 'equipment', 'shift_hours', 'down_hours',
        'operating_hours', 'chargable_hours', 'rental_cost',
    )
    search_fields = [
        'date_used', 'equipment__propertyno',
    ]
    date_hierarchy = 'date_used'

    def get_form(self, request, obj=None, **kwargs):
        if not request.user.is_superuser:
            self.exclude = (
                'previous_operating_hours',
                'rental_cost',
            )
        form = super(UtilizationAdmin, self).get_form(
            request, obj, **kwargs
        )
        return form


class MaintenanceTypeAdmin(admin.ModelAdmin):
    list_display = (
        '__str__', 'type_code', 'equipment_type',
        'schedule_type', 'operating_hours_threshold',
        'mileage_threshold',
    )
    search_fields = [
        'type_name', 'equipment_type__type_name',
    ]

class MaintenancePlanAdmin(admin.ModelAdmin):
    list_display = (
        'equipment',
        'date_start',
        'date_end',
    )
    search_fields = [
        'equipment__propertyno',
    ]


class MaintenanceAdmin(admin.ModelAdmin):
    list_display = (
        'equipment',
        'date_next',
        'last_operating_hours',
        'last_mileage_reading',
    )
    search_fields = [
        'equipment__propertyno',
    ]

    # def get_status(self, obj):
    #     status = 'Good'
    #     mileage = obj.equipment.total_mileage
    #     hours = obj.equipment.total_operating_hours
    #
    #     mileage -= obj.last_mileage_reading
    #     hours -= obj.last_operating_hours
    #
    #     # if mileage >= obj.mileage_threshold:
    #     #     status = 'FOR MAINTENANCE'
    #     # elif mileage > obj.mileage_threshold_warn:
    #     #     status = 'WARNING'
    #     #
    #     # if hours >= obj.operating_hours_threshold:
    #     #     status = 'FOR MAINTENANCE'
    #     # elif hours > obj.operating_hours_threshold_warn:
    #     #     status = 'WARNING'
    #
    #     reading = 0
    #     if mileage > reading:
    #         reading = "%d kph/mph" % (mileage)
    #     if hours > reading:
    #         reading = "%d hours" % (hours)
    #
    #     return "%s ( %s )" % (status, reading)
    #
    # get_status.short_description = 'Maintenace'


class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        'project_code', 'project_title', 'date_start',
    )
    search_fields = [
        'project_title', 'project_code',
    ]

class DeploymentAdmin(admin.ModelAdmin):
    list_display = (
        '__str__',
    )
    search_fields = [
        'project__project_title', 'project__project_code',
    ]

admin.site.register(Equipment, EquipmentAdmin)

admin.site.register(EquipmentType, EquipmentTypeAdmin)
admin.site.register(EquipmentModel, EquipmentModelAdmin)
admin.site.register(EquipmentStatus)

admin.site.register(Location, LocationAdmin)
admin.site.register(Supplier, SupplierAdmin)

admin.site.register(JobOrder, JobOrderAdmin)

admin.site.register(Utilization, UtilizationAdmin)

admin.site.register(MaintenanceType, MaintenanceTypeAdmin)
admin.site.register(MaintenancePlan, MaintenancePlanAdmin)
admin.site.register(Maintenance, MaintenanceAdmin)

admin.site.register(Projects, ProjectAdmin)
admin.site.register(Deployments, DeploymentAdmin)

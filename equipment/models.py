from __future__ import unicode_literals

from datetime import datetime, date, timedelta

from django.db import models

from django.db.models import Count, Sum, F, FloatField, Max

def delta_month():
    return datetime.now() + timedelta(days = 30)

def delta_year():
    return datetime.now() + timedelta(days = 365)

class EquipmentManager(models.Manager):
    """
    Custom Manager developed to automate most of the queries for the project.
    Some queries looks the same and could be done without the need for the
    manager, or by using a combination of other functions. But this allows for
    a faster processing time.
    """

    def get_active(self, check):
        """
        Retrieves all currently active equipment. By definition, an active
        equipment is one that is not in any storage facility, but on-site.
        It also filters it by 'check' to see if relevant records exist.

        Assumes:
        - That the location_type for In-Storage locations is still 'STORED'.
        - That 'check' is a valid equipment.models class that is directly
            connected to the 'Equipment' model.

        Arguments:
        - 'check' is the name, or list of names, of the model which will be
            checked for any records. Any entry in the Equipment table that has
            no corresponding record in the 'check' table will be excluded.

        Returns:
        - An empty list if no records meet all the criteria.
        - A QuerySet of all equipment meeting the said criteria.
        """
        # Change the 'location_type' to the appropriate 'In-Storage' value.
        e = super(EquipmentManager, self).\
            exclude(location__location_type='STORED')

        if 'utilization' in check:
            e = e.annotate(count=Count('utilization'))

        if 'maintenance' in check:
            e = e.annotate(count=Count('maintenance'))
        # Add additional checks here.

        e = e.exclude(count=0)
        return e


    def get_col_totals(self, **targets):
        return [
            super(EquipmentManager,self).
            aggregate(**{k:Sum(v)})
            for k, v in targets.items()
        ]

    def get_active_utilization(self):
        return super(EquipmentManager, self).\
            exclude(location__location_type='STORED').\
            annotate(count=Count('utilization')).\
            exclude(count=0)

class MaintenanceManager(models.Manager):
    """
    Custom Manager developed to automate most of the queries for the project.
    Some queries looks the same and could be done without the need for the
    manager, or by using a combination of other functions. But this allows for
    a faster processing time.
    """

    def get_maintenance_current(self, eid):
        """
        Retrieves the last Preventive Maintenance record for the equipment in
        'eid'.

        Assumes:
        - 'eid' is a list of active 'equipment id' with Preventive Maintenance
            Records. Try using the 'aux' manager's get_active('maintenance')
            method to make sure that only active equipment with Preventive
            Maintenance Records are passed.

        Arguments:
        - 'eid' is a list of valid 'equipment id'.

        Returns:
        - An empty list if there are no Maintenance Records.
        - A list containing details of the Maintenance Record.
        """

        m = super(MaintenanceManager,self).filter(
            equipment__in=eid
        )

        if len(m) > 0:
            m = m.latest()

        return m

class UtilizationManager(models.Manager):
    """
    Custom Manager developed to automate most of the queries for the project.
    Some queries looks the same and could be done without the need for the
    manager, or by using a combination of other functions. But this allows for
    a faster processing time.
    """
    def get_utilization_summary(self, eset, start, end):
        """
        Retrieves a list of dictionary type records for the Utilization Summary
        of the Equipment in eset.

        Assumes:
        - 'eset' is a list of 'active' equipment. Try using the 'aux' manager's
            get_active_utilization() or get_active('utilization') methods
            to make sure that only active equipment with Utilization Records
            are passed.

        Arguments:
        - 'eset' is a list of QuerySet or Dictionary that must have an
            'equipment' attribute or item.
        - 'start' and 'end' are of type 'datetime'.

        Returns:
        - An empty list if no records are found.
        - A QuerSet of Dictionaries containing the annotated fields grouped by
            the equipment's id. Key names are as follows:

            - 'eid' ; equipment id
            - 'pn'  : property no
            - 'tsh' : total shift hours
            - 'tdh' : total down hours
            - 'toh' : total operating hours
            - 'tch' : total chargable hours
            - 'tah' : total available hours (computed: tsh - tdh)
            - 'tih' : total idle hours (computed: tah - toh)
            - 'pah' : percentage of hours available (computed: tah / tsh)
            - 'pdh' : percentage of hours down (computed: tdh / tsh)
            - 'puh' : percentage of hours utilized (computed: toh / tah)
            - 'pih' : percentage of hours idle (computed: tih / tah)

        """
        u = super(UtilizationManager,self).filter(
            equipment__in=eset,
            date_used__range=(start,end)
        ).\
        values('equipment').\
        annotate(
            eid=F('equipment'),\
            pn=F('equipment__propertyno'),\
            tsh=Sum('shift_hours', output_field=FloatField()),\
            tdh=Sum('down_hours', output_field=FloatField()),\
            toh=Sum('operating_hours', output_field=FloatField()),\
            tch=Sum('chargable_hours', output_field=FloatField()),\
            tah=F('tsh')-F('tdh')\
        )

        u = u.annotate(\
            tih=F('tah')-F('toh')\
        )
        u = u.annotate(\
            pah=F('tah') * 100.00 / F('tsh'),\
            pdh=F('tdh') * 100.00 / F('tsh'),\
            puh=F('toh') * 100.00 / F('tah'),\
            pih=F('tih') * 100.00 / F('tah')\
        ).\
        values(
            'eid','pn','tsh','tdh','toh','tch',\
            'tah','tih','pah','pdh','puh','pih'\
        )

        return u

    def get_utilization_period(self, uid, start, end):
        """
        Retrieves the Utilization History of the Equipment specified by 'uid'
        from 'start' date to 'end' date.

        Assumes:
        - 'uid' is the 'equipment id' of a valid Equipment with a Utilization
            History record. Usually retrieved from the result of an Equipment's
            aux.get_active('utilization') method.

        Arguments:
        - 'uid' is an integer.
        - 'start' and 'end' are of type 'datetime'.

        Returns:
        - An empty list if no records are found.
        - A QuerSet of Dictionaries containing the annotated fields grouped by
            the equipment's id. Key names are as follows:

            - 'id'  : utilization id
            - 'ded' : dedupid of the record
            - 'dud' : date used
            - 'tsh' : total shift hours
            - 'tdh' : total down hours
            - 'toh' : total operating hours
            - 'tch' : total chargable hours
            - 'tah' : total available hours (computed: tsh - tdh)
            - 'tih' : total idle hours (computed: tah - toh)

        """
        u = super(UtilizationManager,self).filter(
            equipment=uid,
            date_used__range=(start,end)
        )

        if len(u) > 0:
            u = u.annotate(
                ded=F('dedupid'),
                dud=F('date_used'),
                tsh=F('shift_hours'),
                tdh=F('down_hours'),
                toh=F('operating_hours'),
                tch=F('chargable_hours'),
                tah=F('shift_hours')-F('down_hours'),
                tih=F('shift_hours')-F('down_hours')-F('operating_hours'),
            ).values(
                'id','ded','dud','tsh','tdh','tah','toh','tih','tch'
            )
        else:
            u = []

        return u

    def get_utilization_maintenance(self, uid):
        """
        Retrieves the Utilization History of the Equipment specified by 'uid'
        for its latest Preventive Maintenance record.

        Assumes:
        - 'uid' is a list of 'equipment id' of valid Equipment with Utilization
            History and Preventive Maintenance records. Usually retrieved from
            the result of an Equipment's aux.get_active([]'utilization',
            'maintenance']) method.

        Arguments:
        - 'uid' is a list that is itiratable.

        Returns:
        - An empty list if no records are found.
        - A QuerSet of Dictionaries containing the annotated fields grouped by
            the equipment's id. Key names are as follows:

            - 'eid' ; equipment id
            - 'pn'  : property no
            - 'tsh' : total shift hours
            - 'tdh' : total down hours
            - 'toh' : total operating hours
            - 'tch' : total chargable hours
            - 'tah' : total available hours (computed: tsh - tdh)
            - 'tih' : total idle hours (computed: tah - toh)
            - 'pah' : percentage of hours available (computed: tah / tsh)
            - 'pdh' : percentage of hours down (computed: tdh / tsh)
            - 'puh' : percentage of hours utilized (computed: toh / tah)
            - 'pih' : percentage of hours idle (computed: tih / tah)

        """
        u = super(UtilizationManager,self).annotate(
            date_last=Max('equipment__maintenance__date_last'),
            date_next=Max('equipment__maintenance__date_next')
        ).filter(
            equipment__in=uid,
            date_used__gt=F('date_last'),
            date_used__lt=F('date_next')
        )

        u = super(UtilizationManager,self).filter(
            id__in=u
        ).values('equipment'
        ).annotate(
            eid=F('equipment'),\
            pn=F('equipment__propertyno'),\
            tsh=Sum('shift_hours', output_field=FloatField()),\
            tdh=Sum('down_hours', output_field=FloatField()),\
            toh=Sum('operating_hours', output_field=FloatField()),\
            tch=Sum('chargable_hours', output_field=FloatField()),\
            tah=F('tsh')-F('tdh'),\
            oht=F('equipment__maintenanceplan__operating_hours_threshold'),\
            ohtw=F('equipment__maintenanceplan__operating_hours_threshold_warn'),\
            mt=F('equipment__maintenanceplan__mileage_threshold'),\
            mtw=F('equipment__maintenanceplan__mileage_threshold_warn')
        )

        u = u.annotate(\
            tih=F('tah')-F('toh')\
        )
        u = u.annotate(\
            pah=F('tah') * 100.00 / F('tsh'),\
            pdh=F('tdh') * 100.00 / F('tsh'),\
            puh=F('toh') * 100.00 / F('tah'),\
            pih=F('tih') * 100.00 / F('tah')\
        ).\
        values(
            'eid','pn','tsh','tdh','toh','tch',\
            'tah','tih','pah','pdh','puh','pih'\
        )

        return u


class EquipmentType(models.Model):
    type_code = models.CharField(
        max_length=32, blank=False,
        unique=True
    )
    type_name = models.CharField(
        max_length=32, blank=False,
        unique=True
    )
    description = models.TextField(
        max_length=1024, null=True, blank=True
    )

    def __str__(self):
        return "%s" % (self.type_name)


class EquipmentModel(models.Model):
    unit_brand = models.CharField(
        max_length=64, blank=False, default="Brand"
    )
    unit_model = models.CharField(
        max_length=512, blank=False,
        unique=True, default="Model"
    )

    equipment_type = models.ForeignKey(
        EquipmentType, on_delete=models.CASCADE,
        null=True, blank=True
    )

    description = models.TextField(
        max_length=1024, null=True, blank=True
    )

    def __str__(self):
        return "%s" % (self.unit_model)


class EquipmentStatus(models.Model):
    status_code = models.CharField(
        max_length=8, blank=False,
        unique=True, default="Code"
    )
    status_name = models.CharField(
        max_length=32, blank=False,
        unique=True, default="Name"
    )
    description = models.TextField(
        max_length=1024, null=True, blank=True
    )

    def __str__(self):
        return "%s" % (self.status_name)

    class Meta:
        verbose_name_plural = "Equipment statuses"


class Location(models.Model):
    LOCATION_TYPE = (
        ('STORED', 'In-Storage'),
        ('ONSITE', 'On-Site'),
    )

    DEFAULT_LOCATION = 'STORED'

    location_code = models.CharField(
        max_length=16, blank=False,
        unique=True, default="Code"
    )
    location_name = models.CharField(
        max_length=254, blank=False,
        unique=True, default="Name"
    )
    location_type = models.CharField(
        max_length=6,
        choices=LOCATION_TYPE,
        default=DEFAULT_LOCATION
    )
    description = models.TextField(
        max_length=1024, null=True, blank=True
    )

    class Meta:
        ordering = ['location_name', 'location_code']

    def __str__(self):
        return "%s" % (self.location_name)


class Supplier(models.Model):
    supplier_code = models.CharField(
        max_length=16, blank=False,
        unique=True, default="Supplier Code",
        help_text="%s" % (
            "Supplier Code / Identity (Internal)",
        )
    )
    supplier_name = models.CharField(
        max_length=64, blank=False,
        unique=True
    )
    account_manager = models.CharField(
        max_length=512, blank=False, default="sales executive",
        help_text="%s" % (
            "Point of Contact",
        )
    )
    contact_number = models.CharField(
        max_length=512, null=True, blank=True
    )
    email = models.CharField(
        max_length=512, null=True, blank=True
    )
    address = models.TextField(
        max_length=512, null=True, blank=True
    )
    permit_number = models.CharField(
        max_length=64, blank=False, unique=True,
        default="local-permit",
        help_text="%s" % (
            "Registered Business Permit",
        )
    )

    def __str__(self):
        return "%s" % (self.supplier_name)


class EngineModel(models.Model):
    engine_brand = models.CharField(
        max_length=32, blank=False
    )
    engine_model = models.CharField(
        max_length=32, blank=False,
        unique=True
    )
    description = models.TextField(
        max_length=1024, null=True, blank=True
    )

    class Meta:
        ordering = ['engine_brand', 'engine_model']

    def __str__(self):
        return "%s (%s)" % (self.engine_model, self.engine_brand)


class Equipment(models.Model):
    propertyno = models.CharField(
        max_length=32, blank=False,
        unique=True
    )
    unit_model = models.ForeignKey(
        EquipmentModel, on_delete=models.CASCADE
    )
    unit_serial_no = models.CharField(max_length=128)

    purchase_cost = models.DecimalField(
        max_digits=16, decimal_places=2
    )
    supplier = models.ForeignKey(
        Supplier, on_delete=models.CASCADE, null=True
    )

    location = models.ForeignKey(
        Location, on_delete=models.CASCADE,
        null=True, blank=True
    )
    status = models.ForeignKey(
        EquipmentStatus, on_delete=models.CASCADE
    )

    total_operating_hours = models.IntegerField(
        default=0,
        help_text="%s" % (
            "Total Operating Hours Used",
        )
    )
    total_mileage = models.IntegerField(
        default=0,
        help_text="%s" % (
            "Total Mileage Run",
        )
    )

    rental_rate = models.DecimalField(
        default=0, max_digits=16, decimal_places=2
    )

    third_party_inspection_certificate = models.CharField(
        default='NOT_CERTIFIED', max_length=64
    )

    description = models.TextField(
        max_length=1024, null=True, blank=True
    )

    objects = models.Manager()
    aux = EquipmentManager()

    def __str__(self):
        return "%s" % (self.propertyno)


class JobOrder(models.Model):
    job_order_no = models.CharField(
        max_length=32, blank=False,
        unique=True
    )
    date_file = models.DateField(
        default=datetime.now, blank=False
    )
    equipment = models.ForeignKey(
        Equipment, on_delete=models.CASCADE,
        null=True
    )
    summary = models.TextField(
        max_length=256, blank=False, default="Summary"
    )
    activities = models.TextField(
        max_length=1024, null=True, blank=True
    )
    date_start = models.DateField(
        null=True, blank=True
    )
    date_completed = models.DateField(
        null=True, blank=True
    )

    def __str__(self):
        return "%s" % (self.job_order_no)


class Projects(models.Model):
    project_code = models.CharField(
        max_length=16, blank=False,
        unique=True, default="PRJ"
    )
    project_title = models.CharField(
        max_length=16, blank=False,
        unique=True, default="Project : "
    )
    date_start = models.DateField(
        default=datetime.now, blank=False,
        help_text="%s" % (
            "Date when project is scheduled to commence.",
        )
    )
    date_end = models.DateField(
        null=True, blank=True,
        help_text="Date when maintenance will be completed"
    )
    location = models.ForeignKey(
        Location, on_delete=models.CASCADE
    )
    description = models.TextField(
        max_length=1024, null=True, blank=True
    )

    class Meta:
        verbose_name_plural = "Projects"

class Deployments(models.Model):
    project = models.ForeignKey(
        Projects, on_delete=models.CASCADE,
        null=True, blank=True
    )
    locations = models.ForeignKey(
        Location, on_delete=models.CASCADE,
        null=True, blank=True
    )

    def __str__(self):
        return "%s" % (self.project__project_title)

class Utilization(models.Model):
    date_used = models.DateField(
        default=date.today, blank=False
    )
    equipment = models.ForeignKey(
        Equipment, on_delete=models.CASCADE
    )
    shift_hours = models.IntegerField(default=0)
    down_hours = models.IntegerField(default=0)
    operating_hours = models.IntegerField(default=0)

    chargable_hours = models.IntegerField(default=0)

    previous_operating_hours = models.IntegerField(default=0)
    cummulative_operating_hours = models.IntegerField(default=0)
    last_mileage_reading = models.IntegerField(default=0)

    rental_rate = models.DecimalField(
        default=0, max_digits=16, decimal_places=2,
        help_text="%s" % (
            "Set Rental Rate to 0 to use the current equipment rate"
        )
    )
    rental_cost = models.DecimalField(
        default=0, max_digits=16, decimal_places=2
    )

    description = models.TextField(
        max_length=1024, null=True, blank=True
    )

    dedupid = models.CharField(
        max_length=64,
        default=datetime.now, blank=False
    )

    objects = models.Manager()
    aux = UtilizationManager()


    class Meta:
        verbose_name_plural = "Utilization History"

    def __str__(self):
        return "%s | %s" % (
            self.equipment,
            self.date_used
        )

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser:
            return super(Utilization, self).get_readonly_fields(
                request, obj
            )
        else:
            return (
                'previous_operating_hours',
                'cummulative_operating_hours',
                'rental_cost',
                'dedupid',
            )

    def save(self, force_insert=False, force_update=False):
        newdedupid = "%s|%s" % (
            self.equipment.propertyno,
            self.date_used
        )
        self.dedupid = newdedupid
        if self.rental_rate < 1:
            self.rental_rate = self.equipment.rental_rate
        self.rental_cost = self.chargable_hours * self.rental_rate
        super(Utilization, self).save(force_insert, force_update)


class MaintenanceType(models.Model):
    SCHEDULE_TYPE = (
        ('MLE', 'Mileage'),
        ('HRS', 'Operating Hours'),
        ('DLY', 'Daily'),
        ('WKL', 'Weekly'),
        ('MTH', 'Monthly'),
        ('ANM', 'Annually'),
    )
    DEFAULT_TYPE = 'HRS'

    type_code = models.CharField(
        max_length=32, blank=False
    )
    type_name = models.CharField(
        max_length=32, blank=False
    )
    description = models.TextField(
        max_length=1024, null=True, blank=True
    )

    equipment_type = models.ForeignKey(
        EquipmentType, on_delete=models.CASCADE
    )

    schedule_type = models.CharField(
        max_length=3, blank=False,
        choices=SCHEDULE_TYPE,
        default=DEFAULT_TYPE
    )

    operating_hours_threshold_warn = models.IntegerField(
        default=0,
        help_text="%s %s" % (
            "Operating hours difference from current and last use",
            "( WARNING )"
        )
    )
    operating_hours_threshold = models.IntegerField(
        default=0,
        help_text="%s %s" % (
            "Operating hours difference from current and last use",
            "( DUE FOR MAINTENANCE )"
        )
    )

    mileage_threshold_warn = models.IntegerField(
        default=0,
        help_text="%s %s" % (
            "Mileage difference from current and last reading",
            "( WARNING )"
        )
    )
    mileage_threshold = models.IntegerField(
        default=0,
        help_text="%s %s" % (
            "Mileage difference from current and last reading",
            "( DUE FOR MAINTENANCE )"
        )
    )

    def __str__(self):
        return "%s" % (self.type_name)


class MaintenancePlan(models.Model):
    equipment = models.ForeignKey(
        Equipment, on_delete=models.CASCADE
    )

    # Must make sure this is for the appropriate equipment type only.
    maintenance_type = models.ForeignKey(
        MaintenanceType, on_delete=models.CASCADE
    )

    date_start = models.DateField(
        default=date.today, blank=False,
        help_text="%s | %s" % (
            "Date when maintenance scheduling will commence.",
            "Used to set the next maintenance date automatically"
        )
    )

    date_end = models.DateField(
        null=True, blank=True,
        default=delta_year,
        help_text="Date when maintenance will be discontinued"
    )

    operating_hours_threshold_warn = models.IntegerField(
        default=0,
        help_text="%s %s" % (
            "Operating hours difference from current and last use",
            "( WARNING )"
        )
    )

    operating_hours_threshold = models.IntegerField(
        default=0,
        help_text="%s %s" % (
            "Operating hours difference from current and last use",
            "( DUE FOR MAINTENANCE )"
        )
    )

    mileage_threshold_warn = models.IntegerField(
        default=0,
        help_text="%s %s" % (
            "Mileage difference from current and last reading",
            "( WARNING )"
        )
    )
    mileage_threshold = models.IntegerField(
        default=0,
        help_text="%s %s" % (
            "Mileage difference from current and last reading",
            "( DUE FOR MAINTENANCE )"
        )
    )

    description = models.TextField(
        max_length=1024, null=True, blank=True
    )

    dedupid = models.CharField(
        max_length=64, unique=True,
        default=datetime.now, blank=False
    )

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser:
            return super(MaintenancePlan, self).get_readonly_fields(
                request, obj
            )
        else:
            return (
                'dedupid',
            )

    def save(self, *args, **kwargs):
        newdedupid = "%s|%s" % (
            self.equipment.propertyno,
            self.maintenance_type.type_code
        )
        self.dedupid = newdedupid

        if self.operating_hours_threshold < 1:
            self.operating_hours_threshold = self.maintenance_type.operating_hours_threshold
        if self.operating_hours_threshold_warn < 1:
            self.operating_hours_threshold_warn = self.maintenance_type.operating_hours_threshold_warn

        if self.mileage_threshold < 1:
            self.mileage_threshold = self.maintenance_type.mileage_threshold
        if self.mileage_threshold_warn < 1:
            self.mileage_threshold_warn = self.maintenance_type.mileage_threshold_warn

        super(MaintenancePlan, self).save(*args, **kwargs)

    def __str__(self):
        return "%s (%s)" % (self.maintenance_type, self.equipment)

class Maintenance(models.Model):
    equipment = models.ForeignKey(
        Equipment, on_delete=models.CASCADE
    )

    date_last = models.DateField(
        null=True, blank=True,
        default=date.today,
        help_text="%s | %s" % (
            "Date when maintenance was last performed",
            "Used to set the next maintenance date automatically"
        )
    )
    date_next = models.DateField(
        null=True, blank=True,
        default=delta_month,
        help_text="%s | %s" % (
            # Modified the text for this project only.
            "Schedule for next Preventive Maintenance",
            "Override date when necessary"
        )
    )

    last_operating_hours = models.IntegerField(
        default=0,
        help_text="%s | %s" % (
            "Total operating hours from last Preventive Maintenance",
            "Set to -1 to get the last reading before saving"
        )
    )
    last_mileage_reading = models.IntegerField(
        default=0,
        help_text="%s | %s" % (
            "Mileage reading from last maintenance",
            "Set to -1 to get the last reading before saving"
        )
    )
    passed_test = models.BooleanField(
        default=True,
        help_text="%s | %s" % (
            "Passed or Failed the Preventive Maintenance",
            "Default is True"
        )
    )

    dedupid = models.CharField(
        max_length=64, unique=True,
        default=datetime.now, blank=False
    )

    objects = models.Manager()
    aux = MaintenanceManager()


    def save(self, *args, **kwargs):
        newdedupid = "%s|%s" % (
            self.equipment.propertyno,
            self.date_last
        )
        self.dedupid = newdedupid

        if self.last_mileage_reading < 0:
            self.last_mileage_reading = self.equipment.total_mileage

        if self.last_operating_hours < 0:
            self.last_operating_hours = self.equipment.total_operating_hours

        super(Maintenance, self).save(*args, **kwargs)

    class Meta:
        get_latest_by = 'date_last'

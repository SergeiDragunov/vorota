
provider "aws" {
    region = "ap-southeast-1"
    profile = "ddtki-dev"
    shared_credentials_file  = "~/.aws/credentials"
}

data "aws_ami" "ubuntu" {
    most_recent = true
    filter {
      name = "name"
      values = ["ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
    }
    filter {
      name = "virtualization-type"
      values = ["hvm"]
    }
    owners = ["099720109477"] # Canonical
}

resource "aws_instance" "gatehouse-ddtki-dev" {
    ami = "${data.aws_ami.ubuntu.id}"
    instance_type = "t2.micro"
    tags {
        Name = "gatehouse-ddtki-dev"
        customer = "ddtki"
        project = "gatehouse"
        team = "digital-warlocks"
    }
}

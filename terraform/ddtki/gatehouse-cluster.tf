
provider "aws" {
    region = "ap-southeast-1"
    profile = "ddtki-dev"
}

data "terraform_remote_state" "ddtki-dev" {
    backend = "s3"
    config {
        bucket = "digital-warlocks"
        key = "gatehouse-ddtki/dev-terraform.tfstate"
        region = "ap-southeast-1"
        profile = "ddtki-dev"
    }
}

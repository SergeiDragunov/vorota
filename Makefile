# gatehouse build Makefile

SHELL=/bin/bash

init :
	@find scripts/env -type f -exec chmod u+x {} \;
	@find scripts/init -type f -exec chmod u+x {} \;

init-dev : init
	@scripts/init/dev-setup

provision :
	@find scripts -type f -exec chmod u+x {} \;
	@scripts/env/provision-env

install :
	@scripts/init/prod-setup

develop : init-dev
	@scripts/init/postgresql-dev-setup

test : init-dev
	@scripts/env/run-tests

test-site :
	@scripts/env/start-env .env-dev

migrate : init
	@scripts/env/make-migrate

migrate-dev : init
	@scripts/env/make-migrate .env-dev

migratedb : init
	@scripts/env/run-migrate

migratedb-dev : init
	@scripts/env/run-migrate .env-dev

dumpdata : init
	@scripts/env/dump-data

dumpdata-dev : init
	@scripts/env/dump-data .env-dev

loaddata : init
	@scripts/env/load-data

cleandb-dev :
	@rm -rf .database/*.sqlite3

loaddata-dev : init cleandb-dev migratedb-dev
	@scripts/env/load-data .env-dev

data-archive :
	@mv -v database ~/

publish :
	@git push -u gatehouse master

haul :
	@git pull gatehouse master

deploy : init haul migratedb

.PHONY : clean
clean :
	@rm -rf tmp/
	@rm -rf procfile-*
	@find . -name '*.pyc' -exec rm -rf {} \;
	@find . -name '.DS_Store' -exec rm -rf {} \;

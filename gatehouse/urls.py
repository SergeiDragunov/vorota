"""gatehouse URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url, include
from django.contrib import admin

from django.http import HttpResponseRedirect

from django.contrib.auth.views import login
from django.contrib.auth.views import logout
from django.contrib.auth.decorators import user_passes_test

from django.conf import settings
from django.conf.urls.static import static

login_forbidden = user_passes_test(lambda u: u.is_anonymous(), '/')

admin.site.site_title = "Gatehouse"
admin.site.site_header = "Equipment | Management"
admin.site.index_title = "Equipment Management | Resources"

urlpatterns = [
    url(r'^$', lambda r: HttpResponseRedirect('dashboard/'), name='dashboard'),
    url(r'^secure/login/$', login_forbidden(login), name="login"),
    url(r'^secure/logout/$', logout, name="logout"),
    url(r'^secure/passwordchange/$', logout, name="password-change"),
    url(r'^secure/admin/', admin.site.urls),
    url(r'^secure/admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^dashboard/', include('dashboard.urls')),
    url(r'^equipment/', include('equipment.urls')),
    url(r'^reports/', include('reports.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

// Key Performance Monitoring
$(document).ready( function () {
    if($('#date-start').length){
        $('#date-start').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            gotoCurrent: true
        });
        $('#date-start').datepicker('setDate','-1m +1d');
        var s_date = $('#date-start').val();
        console.log(s_date);
    };

    if($('#date-end').length){
        $('#date-end').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            gotoCurrent: true
        });
        $('#date-end').datepicker('setDate',Date.now());
        var e_date = $('#date-end').val();
        console.log(e_date);
    };


    var kpm_sum = $('#kpm-summary').DataTable(
        {
            'ajax': 'data/?d_start='+ s_date + '&d_end=' + e_date,
            'dom': '<"top"f>rt<"bottom"p><"clear">',
            'displayLength': 25,
            'columns':[
                {'data':'id'},
                {
                    'data':null,
                    'render': function(data, type, full, meta){
                        return '<a href="' +
                            data.id +
                            '/'+ data.drs + '/' + data.dre + '/">' +
                            data.pn  + '</a>';
                    }
                },
                {'data':'tsh'},
                {'data':'tdh'},
                {'data':'tah'},
                {'data':'toh'},
                {'data':'tih'},
                {'data':'pah'},
                {'data':'pdh'},
                {'data':'puh'},
                {'data':'pih'}
            ],
            'columnDefs' : [
                {
                    'targets': [ 0 ],
                    'visible': false,
                    'searchable': true
                }
            ]
        }
    );

    console.log('updated table');

    if($('#get-data').length){
    $('#get-data').click(
        function(){
        s_date = $('#date-start').val();
        e_date = $('#date-end').val();
        console.log(s_date);
        console.log(e_date);
        kpm_sum.ajax.url('data/?d_start='+ s_date + '&d_end=' + e_date).load();
    });};
    console.log('added listener');

    $('#kpm-detail').DataTable(
        {
            'ajax': {
                'url':'data/',
                'dataSrc':'summary'
            },
            'dom': 'rt<"clear">',
            'displayLength': 35,
            'columns':[
                {'data':'pn'},
                {'data':'tsh'},
                {'data':'tdh'},
                {'data':'tah'},
                {'data':'toh'},
                {'data':'tih'},
                {'data':'pah'},
                {'data':'pdh'},
                {'data':'puh'},
                {'data':'pih'}
            ]
        }
    );

    $('#kpm-utilization').DataTable(
        {
            'ajax': {
                'url':'data/',
                'dataSrc':'utilization'
            },
            'dom': 'rt<"bottom"p><"clear">',
            'displayLength': 35,
            'columns':[
                {'data':'id'},
                {'data':'ded'},
                {'data':'dud'},
                {'data':'tsh'},
                {'data':'tdh'},
                {'data':'tah'},
                {'data':'toh'},
                {'data':'tih'},
                {'data':'tch'}
            ],
            'columnDefs' : [
                {
                    'targets': [ 0 ],
                    'visible': false,
                    'searchable': true
                }
            ]
        }
    );

} );

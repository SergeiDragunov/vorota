/* reports/billing */

$(document).ready( function () {
    $('#equipment201').DataTable(
        {
            ajax: 'data/list/',
            dom: 'frtip',
            'columns':[
                {'data':'id'},
                {
                    'data':null,
                    'render': function(data, type, full, meta){
                        return '<a href="details/' + data.id + '/">' +
                            data.pn  + '</a>';
                    }
                },
                {'data':'mn'},
                {'data':'sn'},
                {'data':'sc'},
                {'data':'lc'},
            ],
            'columnDefs' : [
                {
                    'targets': [ 0 ],
                    'visible': false,
                    'searchable': true
                }
            ]
        }
    );

    $('#201-detail-1').DataTable(
        {
            'ajax': {
                'url':'data/',
                'dataSrc':'detail'
            },
            'dom': 'rt<"clear">',
            'columns':[
                {'data':'id'},
                {'data':'pn'},
                {'data':'mn'},
                {'data':'sn'},
                {'data':'sc'},
                {'data':'lc'},
            ],
            'columnDefs' : [
                {
                    'targets': [ 0 ],
                    'visible': false,
                    'searchable': true
                }
            ]
        }
    );

    $('#201-detail-2').DataTable(
        {
            'ajax': {
                'url':'data/',
                'dataSrc':'detail'
            },
            'dom': 'rt<"clear">',
            'columns':[
                {'data':'suc'},
                {'data':'pc'},
                {'data':'rr'},
                {'data':'tpc'},
                {'data':'toh'},
                {'data':'tmr'},
            ]
        }
    );

    $('#201-utilization').DataTable(
        {
            'ajax': {
                'url':'data/',
                'dataSrc':'utilization'
            },
            'dom': 'rt<"bottom"p><"clear">',
            'displayLength': 35,
            'columns':[
                {'data':'id'},
                {'data':'ded'},
                {'data':'dud'},
                {'data':'tsh'},
                {'data':'tdh'},
                {'data':'tah'},
                {'data':'toh'},
                {'data':'tih'},
                {'data':'tch'}
            ],
            'columnDefs' : [
                {
                    'targets': [ 0 ],
                    'visible': false,
                    'searchable': true
                }
            ]
        }
    );

    $('#201-joborder').DataTable(
        {
            'ajax': {
                'url':'data/',
                'dataSrc':'joborder'
            },
            'dom': 'rt<"bottom"p><"clear">',
            'displayLength': 35,
            'columns':[
                {'data':'id'},
                {'data':'jon'},
                {'data':'dc'},
                {'data':'jos'},
                {'data':'joa'},
                {'data':'ds'},
                {'data':'df'}
            ],
            'columnDefs' : [
                {
                    'targets': [ 0 ],
                    'visible': false,
                    'searchable': true
                }
            ]
        }
    );

    $('#201-pmplan').DataTable(
        {
            'ajax': {
                'url':'data/',
                'dataSrc':'pmplan'
            },
            'dom': 'rt<"bottom"p><"clear">',
            'displayLength': 3,
            'columns':[
                {'data':'id'},
                {'data':'ded'},
                {'data':'dps'},
                {'data':'dpe'},
                {'data':'oht'},
                {'data':'ohtw'},
                {'data':'mt'},
                {'data':'mtw'}
            ],
            'columnDefs' : [
                {
                    'targets': [ 0 ],
                    'visible': false,
                    'searchable': true
                }
            ]
        }
    );

    $('#201-pmrecord').DataTable(
        {
            'ajax': {
                'url':'data/',
                'dataSrc':'pmrecord'
            },
            'dom': 'rt<"bottom"p><"clear">',
            'displayLength': 35,
            'columns':[
                {'data':'id'},
                {'data':'ded'},
                {'data':'ppm'},
                {'data':'npm'},
                {'data':'poh'},
                {'data':'pmr'},
                {'data':'pmp'}
            ],
            'columnDefs' : [
                {
                    'targets': [ 0 ],
                    'visible': false,
                    'searchable': true
                }
            ]
        }
    );

    $('#tabs').tabs();

} );

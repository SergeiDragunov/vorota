value_min = 10000.00;
rate_cubic_meter = 200;
rate_level = 20;

function CalculateMixer(){
    flr_lvl = $("#flr_lvl").val();
    flr_vol = $("#flr_vol").val();

    if ((flr_vol * rate_cubic_meter) > value_min) {
        value_volume = flr_vol * rate_cubic_meter;
    } else {
        value_volume = value_min;
    }

    if (flr_lvl > 1){
        value_level = flr_lvl * rate_level;
    } else {
        value_level = 0;
    }

    total_cost = value_level + value_volume;
    out_string = "The estimated cost for pumping concrete with total volume of " + flr_vol +
    " cubic meter/s on level " + flr_lvl + " is " + total_cost + " Pesos.";
    $("#computation_result").text(out_string);

}

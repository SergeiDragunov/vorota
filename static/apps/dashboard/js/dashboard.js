$(document).ready( function () {
    var cdata = $.ajax({
        url: "data/?type=count",
        dataType: "json",
        async: false
    }).responseText;

    console.log(cdata)

    var tdata = $.ajax({
        url: "data/?type=total",
        dataType: "json",
        async: false
    }).responseText;

    console.log(tdata)

    var xdata = $.ajax({
        url: "data/?type=topx",
        dataType: "json",
        async: false
    }).responseText;

    console.log(xdata)

    var pdata = $.ajax({
        url: "data/?type=topp",
        dataType: "json",
        async: false
    }).responseText;

    console.log(pdata);

    // Load the Visualization API and the corechart package.
    // Edited: load bar package for Materials chart.
    google.charts.load('current', {'packages':['corechart','bar']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawChart);

    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function drawChart() {

    // Create the datatables.
    var data1 = new google.visualization.DataTable(cdata);
    var data2 = new google.visualization.DataTable(tdata);
    var data3 = new google.visualization.DataTable(xdata);
    var data4 = new google.visualization.DataTable(pdata);

    // Set chart options
    var colors = ['#6AFB92','#F75D59','#779ECB','#FFF380'];

    var avail_options = {'title':'EQUIPMENT AVAILABILITY',
        'width':600,
        'height':500,
        'legend':{'position': 'bottom', },
        'colors':colors,
        'pieHole':0.4,
        'fontName':'Arial, sans serif',
        'fontSize': 13,
        'titleTextStyle': {
            color: '#717164',
            fontName: 'Arial, sans serif',
            fontSize: 15   ,
            bold: 'False'
        }

    };

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.PieChart(document.getElementById('avail-charts'));
    chart.draw(data1, avail_options);

    // Set chart options
    var usage_options = {'title':'EQUIPMENT USAGE',
        'width':300,
        'height':300,
        'legend':{'position': 'bottom', },
        'colors':colors,
        'pieHole':0.4,
        'fontName': 'Arial, sans serif',
        'fontSize': 13,
        'titleTextStyle': {
            color: '#717164',
            fontName: 'Arial, sans serif',
            fontSize: 15   ,
            bold: 'False'
        }
    };
    //alert(tdata.toString());
    var chart = new google.visualization.PieChart(document.getElementById('usage-charts'));
    chart.draw(data2, usage_options);

    var pm_options = {
        width:850,
        height:500,
        colors: '#F75D59',
        chart:{
            title: 'PREVENTIVE MAINTENANCE MONITORING',
            subtitle: 'By Operating Hours'
        },
        series:{
            0: {target: 'operatinghours'}
        },
        axes:{
            y: {
                operatinghours: {}
            },
        },
        vAxes:{
            0: {
                title: 'HOURS',
                textPosition: 'in',
                ticks: [100, 200, 300, 400, 500],
                viewWindowMode: 'explicit',
                viewWindow:{
                    max: 500
                }
            }
        }
    };

    var chart = new google.charts.Bar(document.getElementById('topx-charts'));
    chart.draw(data3, google.charts.Bar.convertOptions(pm_options));

    var kpm_options = {
        width:850,
        height:500,
        colors: colors,
        chart:{
            title: 'KEY PERFORMANCE MONITORING',
            subtitle: 'Ranked by Utility for latest PM Schedule'
        },
        series:{
            0: {target: 'operatinghours'}
        },
        axes:{
            y: {
                operatinghours: {}
            },
        },
        vAxes:{
            0: {
                title: '% OF TOTAL HOURS',
                textPosition: 'in',
                ticks: [20, 40, 60, 80, 100],
                viewWindowMode: 'explicit',
                viewWindow:{
                    max: 100
                }
            }
        }
    };

    var chart = new google.charts.Bar(document.getElementById('topp-charts'));
    chart.draw(data4, google.charts.Bar.convertOptions(kpm_options));

    // Initialize the slider
    $('.bxslider').bxSlider({
        auto: true,
        speed: 5000,
        autoControls: true
    });

    };
});

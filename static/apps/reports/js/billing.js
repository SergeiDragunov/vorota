/* reports/billing */

$(document).ready( function () {
    var reports = $('#billing-report').DataTable(
        {
            ajax: 'data/',
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'pdfHtml5',
                    message: 'Billing Report'
                }
            ]
        }
    );

} );

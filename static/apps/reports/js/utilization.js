/* reports/utilization */

$(document).ready( function () {
    var reports = $('#utilization-report').DataTable(
        {
            ajax: 'data/',
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'pdfHtml5',
                    message: 'Utilization Report'
                }
            ]
        }
    );

} );

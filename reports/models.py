from __future__ import unicode_literals

from django.db import models

class Reports(models.Model):

    class Meta:
        managed = False

        permissions= (
            ('view_reports', 'Can view reports'),
        )

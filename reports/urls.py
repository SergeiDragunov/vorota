from django.conf.urls import url

from . import views

app_name='reports'
urlpatterns = [
    url(r'^$', views.ReportsView.as_view(), name='index'),
    url(r'^utilization/$', views.UtilizationView.as_view(), name='utilization'),
    url(r'^utilization/data/$', views.UtilizationData, name='utilization-data'),
    url(r'^billing/$', views.BillingView.as_view(), name='billing'),
    url(r'^billing/data/$', views.BillingData, name='billing-data'),
    url(r'^billing/(?P<projectid>[0-9])/$', views.BillingView.as_view()),
    url(r'^billing/(?P<projectid>[0-9])/data/$', views.BillingData),
]

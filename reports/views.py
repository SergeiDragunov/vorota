from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.views.generic import ListView

from django.core import serializers

from django.http import HttpResponse, HttpResponseNotFound

from equipment.models import Utilization

from datetime import datetime

import json

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('reports.view_reports'), name='dispatch')
class ReportsView(TemplateView):
    template_name = 'reports/index.html'

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('reports.view_reports'), name='dispatch')
class UtilizationView(TemplateView):
    template_name = 'reports/utilization.html'

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('reports.view_reports'), name='dispatch')
class BillingView(TemplateView):
    template_name = 'reports/billing.html'


class AJAXListMixin(object):

    def dispatch(self, request, *args, **kwargs):
        if not request.is_ajax():
            raise http.Http404("This is an ajax view, friend.")
        return super(AJAXListMixin, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return (
            super(AJAXListMixin, self)
            .get_queryset()
            .filter(ajaxy_param=self.request.GET.get('some_ajaxy_param'))
        )

    def get(self, request, *args, **kwargs):
        return HttpResponse(serializers.serialize('json', self.get_queryset()))

@method_decorator(login_required, name='dispatch')
class UtilizationList(AJAXListMixin, ListView):
    pass

@method_decorator(login_required, name='dispatch')
class BillingList(AJAXListMixin, ListView):
    pass


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial
    else:
        return obj.__str__()

def UtilizationData(request):
    queryset = Utilization.objects.all()
    resp = {}
    data = [
        [
            item.equipment.location.location_code,
            item.date_used,
            item.equipment.propertyno,
            item.shift_hours,
            item.down_hours,
            item.operating_hours,
            item.chargable_hours,
        ] for item in queryset
    ]
    resp['data'] = data
    return HttpResponse(json.dumps(resp, default=json_serial), content_type='application/json')

def BillingData(request, projectid=0):
    queryset = Utilization.objects.all()
    resp = {}

    data = [
        [
            item.equipment.location.location_code,
            item.date_used,
            item.equipment.propertyno,
            item.rental_rate,
            item.rental_cost,
        ] for item in queryset
    ]
    resp['data'] = data
    return HttpResponse(json.dumps(resp, default=json_serial), content_type='application/json')

# Copyright 2016, Bryan Patrick Luartes
# (aka Sergei Dragunov of Digital Warlocks)
# Released under the MIT license - http://opensource.org/licenses/MIT
# Changelog 2016-10-27: Still no error handling features

import django.db.models
import calendar
import json
import sys

from random import randint
from django.http import HttpResponse, HttpResponseNotFound

from datetime import datetime, date

from django.db.models import Avg, Count, F, Sum
from equipment.models import *

class DatabaseAux:
    # Class for abstracting the Model's Structure while developing,
    # so that changes in the Models can be ignored until finalized.
    def __init__(self):
        self.map_links = {}
        self.map_filters = {}

    def json_serial(self, obj):
        """JSON serializer for objects not serializable by default json code"""
        if isinstance(obj, datetime):
            serial = obj.isoformat()
            return serial
            raise TypeError ("Type not serializable")
        else:
            return obj.__str__()

    def get_timedelta(self, d_start=None, d_end=1):
        if d_start is None:
            d_start = date.today()

        d_year = d_start.year
        d_month = d_start.month + d_end

        if d_end < 0:
            d_day = d_start.day + 1
            if d_month < 1:
                d_year = d_start.year -1
                d_month = 12 + d_month
        elif d_end > 0:
            d_day = d_start.day - 1
            if d_month > 12:
                d_year = d_start.year + 1
                d_month = d_month - 12


        if calendar.monthrange(d_year,d_month)[1] < d_day:
            d_day = calendar.monthrange(d_year,d_month)[1]

        return date(d_year, d_month, d_day)

    def get_ranking(self, source, rank_index, rank_count=10, is_reverse=True):
        #
        # Place all checks on 'source' here.
        new_set = sorted(source, key=lambda k: k[rank_index], reverse=is_reverse)
        if rank_count == 0 or rank_count > len(source):
            return new_set
        else:
            return new_set[0:rank_count]

    def get_pm_matrix(self):
        e_set = Equipment.aux.get_active(['maintenance','utilization'])
        u_set = Utilization.aux.get_utilization_maintenance(e_set)

        u_set = [{
                k: (lambda v: 0 if v is None else v)(v)
                for k, v in u.items()
            }
            for u in u_set
        ]

        return u_set

    def get_clean_data(self,dbdata,newdata='None'):
        if dbdata is None:
            return newdata
        else:
            return dbdata

    def get_kpm_matrix(self, uid=None, d_start=None, d_end=-1):
        if d_start is None:
            d_start = self.get_timedelta(d_end=d_end)

        if d_end == -1:
            d_end = date.today()

        if uid is None:
            uid = Equipment.aux.get_active('utilization')

        e_set = Utilization.aux.get_utilization_summary(
            start=d_start, end=d_end, eset=uid
        )

        e_set = [{
            'id':e['eid'],
            'pn':e['pn'],
            'tsh':e['tsh'],
            'tdh':e['tdh'],
            'toh':e['toh'],
            'tch':e['tch'],
            'tah':e['tah'],
            'tih':e['tih'],
            'pah':"{0:.2f}".format(e['pah']),
            'pdh':"{0:.2f}".format(e['pdh']),
            'puh':"{0:.2f}".format(e['puh']),
            'pih':"{0:.2f}".format(e['pih']),
            'drs':datetime.strftime(d_start, '%Y-%m-%d'),
            'dre':datetime.strftime(d_end, '%Y-%m-%d')
        } for e in e_set ]
        return e_set

    def get_link(self, source, target, key):
        """
        Retrieves the link, if connected, between two models, from 'source' to
        'target'. 'key' is the field in the target model to be used for any
        search or update functions.

        If the 'map_links' definition does not exist, it will cal the
        create_links function to generate the list if it can.
        """
        if source == None and target == None and key == None:
            pass
        else:
            map_link = source.__name__ + "-" + target.__name__
            if len(self.map_links) == 0 or \
                not "_" + map_link in self.map_links or \
                not map_link in self.map_links:
                return_dump = self.create_links(source)
            if "_" + map_link in self.map_links:
                filter_link = self.map_links["_" + map_link] + key
            else:
                filter_link = self.map_links[map_link] + key
            return filter_link

    def get_set(self, source, target, key, value, name=None):
        """
        Retrieves a dataset similar to a SQL SELECT statement.

        'key' is the field for which the 'value' will be tested. 'name' if given
        will replace the default fieldname.
        """

        filter_link = self.get_link(source, target, key)
        if value == None:
            pass
        else:
            if name == None:
                qset = \
                    source.objects.filter(**{filter_link:value})
            else:
                qset = \
                    source.objects.filter(**{filter_link:value}).\
                    annotate(**{name:F(filter_link)})
        return qset

    def get_group(self, target, source, key, name=None):
        """
        Retrieves a dataset similar to a SQL GROUP statement.

        'key' is the field for which the 'value' will be tested. 'name' if given
        will replace the default fieldname.
        """

        filter_link = self.get_link(target, source, key)
        if name == None:
            # # Example:
            # get_link : equipment__maintenance_
            # key : date_last
            # the filter_link will be: equipment__maintenance_date_last

            grouped_set = \
                target.objects.values(filter_link)
        elif type(name) == str:
            # # Example:
            # get_link : equipment__maintenance_
            # key : date_last
            # name : pm_date_last
            # the filter_link will be: equipment__maintenance_date_last
            # but will be renamed: pm_date_last
            grouped_set = \
                target.objects.annotate(**{name:F(filter_link)}).values(name)
        elif type(name) == tuple:
            pass
        return grouped_set

    def get_col_totals(self, source, for_charts=False, **targets):
        """
        Gets the SUM for all the 'target' fields in the given table.
        'targets.key' will be used as the column name.
        'targets.value' is the name of the database fieldname.

        Assumes that the 'targets.value' are numeric data types.
        Assumes 'source' if of type QuerySet.
        Assumes 'targets' is a kwargs.
        """

        qs = {}
        for k, v in targets.items():
            qs.update(source.objects.aggregate(**{k:Sum(v)}))
        if for_charts:
            qs = [
                [' '.join([z for z in [a.upper() for a in
                 k.replace('_',' ').split()]]),
                 v
                ]
                for k, v in qs.items()
            ]
        return qs

    def get_count(self, source, target, key, \
        value, name=None, for_charts=False):
        """
        Works like: SQL SELECT COUNT AS [alias] FROM [table] WHERE [field=value]

        Where:
        source = table
        target = field
        key = value
        name = alias
        """

        filtered_set = self.get_set(source, target, key, value)
        filtered_set = filtered_set.annotate(count=Count(key))
        if for_charts:
            filtered_set = [
                [i[name], i["count"]]
                for i in filtered_set
            ]
        return filtered_set

    def get_counts(self, target, source, key, name=None, for_charts=False):
        """
        'get_count' for multiple fields.
        """

        grouped_count = self.get_group(target, source, key, name)
        grouped_count = grouped_count.annotate(count=Count(name))
        if for_charts:
            grouped_count = [
                [i[name], i["count"]]
                for i in grouped_count
            ]
        return grouped_count

    def get_format(self, dataset, col_names=["DefColName1", "DefColName2"],
            col_types=["string","number"]):
        """
        Formats a given dataset for use with the Google Chart object.

        Default is a simple Chart with a column for the 'label' and one for
        'value'.

        For a 'Material Chart' where you can have multiple values displayed,
        the format of the datalist should be:

        [
            ['Axis-Name','Bar1-Label','Bar2-Label','BarX-Label'],
            ['Axis1-Label','Bar1-Value','Bar2-Value','BarX-Value'],
            ['AxisN-Label','Bar1-Value','Bar2-Value','BarX-Value'],
        ]

        """
        datalist = []
        headerlist = []

        if len(dataset) == 0:
            chancesare = randint(1,500)
            for i in range(len(col_types)):
                headerlist.append({
                    "label": col_names[i].replace("_", " ").upper(),
                    "type": col_types[i]
                })
            datalist = [
                {"c":[{"v":"SimulatedValue"},{"v":chancesare}]},
                {"c":[{"v":"SimulatedValue"},{"v":500 - chancesare}]}
            ]
        else:
            def_col_name = "DefColNameX"
            def_col_type = "number"
            # This part assumes you did not provide any custom header. It will
            # make sure the headerlist is the same length as the datalist.
            while len(col_names) < len(dataset[0]):
                col_names.append(def_col_name)
            while len(col_types) < len(dataset[0]):
                col_types.append(def_col_type)

            # Populate the headerlist
            for i in range(len(col_types)):
                headerlist.append({
                    "label": col_names[i].replace("_", " ").upper(),
                    "type": col_types[i]
                })

            # Populate the datalist
            for dataitem in dataset:
                # Assumes that a standard graph. For a 'Material Chart', proper
                # format should be used.
                rows = [{"v":i} for i in dataitem]
                datalist.append({"c":rows})
        datatable = {
            "cols": headerlist,
            "rows": datalist
        }
        return datatable


    # Create the link strings to all relationship of source
    # Assumes source is an instanced object of the type:
    # django.db.models.base.ModelBase
    def create_links(self, source, rkf=None, modmap=None):
        if (type(source) != django.db.models.base.ModelBase or \
                type(source.__class__.__bases__[0]) != \
                django.db.models.base.ModelBase) and \
                source == None:
            return "something's wrong"
            # Throw exception: source must be an instanced object of type
            # django.db.models.base.ModelBase
        else:
            isendofrelation = []
            mapping = [source]
            name_s = source.__name__
            # Itirate through all the fields
            for f in source._meta.get_fields():
                # Check if fiield is a related one
                if f.is_relation:
                    # Pseudo check if this is a recursive call
                    if rkf != None:
                        # Add all the models that have been mapped to avoid
                        # an infinite loop through all the models.
                        mapping.extend(modmap)
                    target = f.related_model
                    # Check if the target is already in the stack of
                    # recursive calls, or will be called in the future
                    # from another model already in the stack. This is to
                    # prevent an infinite loop forming.
                    if not target in mapping:
                        pass
                        # Calls self recursively. This will return the links,
                        # which will be true in the test
                        if f.many_to_one or f.one_to_many:
                            loc_links = self.create_links(
                                target,                 # source
                                f.target_field.name,    # reverse key field
                                mapping                 # modmap
                            )
                        elif f.many_to_many:
                            loc_links = self.create_links(
                                target,
                                f.m2m_reverse_target_field_name,
                                mapping
                            )
                        # The recursion will take care of spanning the lenght
                        # of the relationships.
                        for i in loc_links:
                            if i[1] == "_":
                                # Insert the connection between adjacent tables.
                                # This is the prime, or shortest connection
                                # between the two models.
                                self.map_links["_" + name_s + "-" + i[0]] = \
                                f.name + "_" + i[1]
                            else:
                                # Insert the connection between adjacent tables
                                self.map_links[name_s + "-" + i[0]] = \
                                f.name + "_" + i[1]
                            # Insert connection between spanned relations
                            isendofrelation.append((
                                i[0],
                                "_" + f.name + "_" + i[1]
                            ))
                # If not is_relation but is the reverse keyfield
                elif f.name == rkf:
                    isendofrelation.append((
                        name_s,
                        "_"
                    )) # Oh God, I hope it isn't
            if rkf != None:
                return isendofrelation

    def test_link(self, source, rfk=None, modmap=None):
        if (type(source) != django.db.models.base.ModelBase or \
                type(source.__class__.__bases__[0]) != \
                django.db.models.base.ModelBase) and \
                source == None:
            return "something's wrong"
            # Throw exception: source must be an instanced object of type
        else:
            print source.__name__
            mapping = [source]
            # Itirate through all the fields
            for f in source._meta.get_fields():
                # Check if fiield is a related one
                if f.is_relation:
                    if rfk != None:
                        mapping.extend(modmap)
                    target = f.related_model
                    # Calls self recursively. This will return the links,
                    # which will be true in the test
                    if f.many_to_one:
                        print f.name + ": M2O: " + f.related_model.__name__
                        print "target: " + target.__name__+"; source: " + source.__name__
                        if not target in mapping:
                            print ">>> calling test_link on "+target.__name__+" from " + source.__name__
                            test_link(target,source,mapping)
                    elif f.many_to_many:
                        print f.name + ": M2M: " + f.related_model.__name__
                        print "target: " + target.__name__+"; source: " + source.__name__
                        if not target in mapping:
                            print ">>> calling test_link on "+target.__name__+" from " + source.__name__
                            test_link(target,source,mapping)
                    elif f.one_to_one:
                        print f.name + ": O2O: " + f.related_model.__name__
                        print "target: " + target.__name__+"; source: " + source.__name__
                        if not target in mapping:
                            print ">>> calling test_link on "+target.__name__+" from " + source.__name__
                            test_link(target,source,mapping)
                    elif f.one_to_many:
                        print f.name + ": O2M: " + f.related_model.__name__
                        print "target: " + target.__name__+"; source: " + source.__name__
                        if not target in mapping:
                            print ">>> calling test_link on "+target.__name__+" from " + source.__name__
                            test_link(target,source,mapping)

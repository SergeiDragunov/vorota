import json

from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator

from django.views.generic import TemplateView, ListView
from django.core import serializers

from django.http import HttpResponse, HttpResponseNotFound
from datetime import datetime
from random import randint

from django.db.models import Sum, Avg, F
from equipment.models import *
from dw.aux import DatabaseAux

class DashboardView(TemplateView):
    template_name = 'dashboard/index.html'

@method_decorator(login_required, name='dispatch')
class CalculatorMixer(TemplateView):
    template_name = 'dashboard/calculator_mixer.html'

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial
        raise TypeError ("Type not serializable")
    else:
        return obj.__str__()

def DashboardCharts(request):
    #set provisions for possible incorporation of request data for selective query
    aux = DatabaseAux()
    argreq = request.GET

    if argreq['type'] == 'count':
        resp = aux.get_counts(
            Equipment,
            EquipmentStatus,
            'status_name',
            'status-name',
            True
        )
        resp = aux.get_format(resp)
    elif argreq['type'] == 'total':
        resp = aux.get_col_totals(
            Utilization,
            True,
            TOTAL_SHIFT_HOURS='shift_hours',
            TOTAL_DOWN_HOURS='down_hours',
        )
        resp = aux.get_format(resp)
    elif argreq['type'] == 'topx':
        pmx = aux.get_pm_matrix()
        pmx = aux.get_ranking(pmx,'toh')
        resp = [[m['pn'],m['toh']] for m in pmx]
        resp = aux.get_format(resp, col_names=['Operating Hours','Property No.'])
    elif argreq['type'] == 'topp':
        pmx = aux.get_pm_matrix()

        if len(pmx) > 0:
            pmx = aux.get_ranking(pmx,'puh')
            resp = [
                [
                    m['pn'],
                    m['pah'] ,
                    m['pdh'],
                    m['puh'],
                    m['pih']
                ]
                for m in pmx
            ]

            resp = aux.get_format(resp,
                col_names=[
                    'Property No.','Availability','Down Time','Utility','Idle'
                ],
                col_types=[
                    'string','number','number','number','number'
                ]
            )
        else:
            resp = aux.get_format([])
    return HttpResponse(json.dumps(resp, default=json_serial), content_type='application/json')

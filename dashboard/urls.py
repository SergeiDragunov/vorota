from django.conf.urls import url

from . import views

app_name = 'dashboard'
urlpatterns = [
    url(r'^$', views.DashboardView.as_view(), name='index'),
    url(r'data/', views.DashboardCharts, name='dashboard-data'),
    url(r'calculator_mixer/', views.CalculatorMixer.as_view(), name='dash-calc-mix'),
]

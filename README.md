# [ project ] gatehouse

![digital-warlocks](https://img.shields.io/badge/digital-warlocks-blue.svg)

#### overview
a gatehouse is a section of a medieval structure such as a castle or fortress that overlooks and controls what comes in and out.

the gatehouse(r) is a platform similar to its reference to oversee and manage resources of the organization.

#### development
since gatehouse(r) functions to secure information and resource for the organization then its appropriate to build the platform with one of the most secure languages used by professionals, python.

with python as the most suitable option then it is best to build the interface with a robust framework for web services, django.

![python-django](https://img.shields.io/badge/python-django-0c4b33.svg)  ![django-version](https://img.shields.io/badge/version-1.10-0c4b33.svg)

#### environment
the project will be designed with the rapid development efficiency and deployment scalability.

the platform can be deployed in these environments:

* development

* testing

* staging

* production

development and testing environments can be launched on developers local machine or on CI/CD tools.

staging and production environments most preferred choice of deployment platforms will be container based to minimize operational overhead.


#### develop locally
developers have 2 options to run and test their instances.

###### on local machines
note: not compatible with windows

requirements:

* foreman

* python

* postgresql


###### on virtual machines
all other prerequisites are provisioned in the virtual machine

requirements:

* vagrant

* virtualbox
